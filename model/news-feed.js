const mongoose= require('mongoose');
var uniqueValidator = require('mongoose-unique-validator')

const newsfeedschema= mongoose.Schema({
    title:{type: String, required: 'Title can\'t be empty', unique:true},
    description:{type: String, required:false},
    link:{type: String, required: 'Link can\'t be empty', unique:true},
    guid:{type: String, required: 'Guid can\'t be empty', unique:true},
    pubDate:{type: String, required:false}
})
newsfeedschema.plugin(uniqueValidator)
module.exports=mongoose.model('NewsFeed', newsfeedschema);